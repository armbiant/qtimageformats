# Copyright (C) 2022 The Qt Company Ltd.
# SPDX-License-Identifier: BSD-3-Clause

# Generated from benchmarks.pro.

if(QT_FEATURE_system_zlib)
    add_subdirectory(mng)
    add_subdirectory(tiff)
endif()
